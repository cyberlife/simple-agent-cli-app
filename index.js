'use strict'
/* eslint-disable no-console */

var cyber = require("cyber-client");
var async = require("async");
const pull = require('pull-stream');
var Web3 = require('web3');
var ipfsUtils = require("./ipfsUtils.js");

var personalNode
var peerList = []
var protocolName = '/cyberlife'

var address = '/ip4/127.0.0.1/tcp/38048'

//HostManagement, AgentsTreasury, AgentVersioning, AgentManagement

var contractsAddresses = [

  "0xfe5d266ddcc87b15184dbaaa466230e560f3e470",
  "0x0b1be75b661a68e77a7051202255ee47ee653fc6",
  "0xc7afa5c89b96f2b12a1a6be5355493583ea42bf8",
  "0xa49b7b3c45ff9481da6dbab17dae78a229a4b4ae"

];

//CONFIGURE NODE

async.parallel([
  (cb) => cyber.createNode(address, cb)
], (err, nodes) => {

  if (err) { throw err }

  personalNode = nodes[0]

  console.log("Created local node")

  personalNode.on('peer:discovery', (peer) => saveNewPeer(peer))

  setTimeout(interact, 5000)

  personalNode.handle(protocolName, (protocol, conn) => {

    pull(
      conn,
      pull.map((data) => {
        return data.toString('utf8').replace('\n', '')
      }),
      pull.drain(console.log)
    )

  })

})

async function interact() {

  console.log("Calling Simple_Agent's functions");

  ipfsUtils.postDatatoIPFS(Buffer.from(JSON.stringify([ [1,2,3,4] ]), 'utf8'),
    function(err, params) {

      var instruction = 'Simple_Agent' + " "
                        + "agent.js" + " "
                        + "computeStuff" + " "
                        + '1' + " ";

      var data = ["execute " + instruction +
        personalNode.peerInfo.id.toB58String() + " "
        + params[0].hash.toString() ];

      dialNode(personalNode, protocolName, peerList[0], data);

  });

  //Need to debug, does not find agent IPFS data
  //cyber.call(personalNode, peerList, "Simple_Agent",
                  //"agent.js", "computeStuff", 1, [1,2,3,4], contractsAddresses);

}

function dialNode(personalNode, protocolName, node, dataArray) {

  if (dataArray.constructor !== Array || dataArray.length == 0) {

    return -1

  }

  personalNode.dialProtocol(node, protocolName, (err, conn) => {

    if (err) {

      return undefined

    }

    pull(pull.values(dataArray), conn)

  })

}

function saveNewPeer(peer) {

  if (!isPeerSaved(peer.id.toB58String())) {

    peerList.push(peer);

  }

}

function isPeerSaved(peerID) {

  for (var i = 0; i < peerList.length; i++) {

    if (peerID == peerList[i].id.toB58String()) return true;

  }

  return false;

}
